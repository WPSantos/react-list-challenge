import { ADD_USER, SET_TEXT_FILTER } from "../actions/actionTypes";

let nextUserId = 0;

export const addUser = content => ({
  type: ADD_USER,
  payload: {
    id: ++nextUserId,
    content
  }
});

export const setTextFilter = filter => ({
  type: SET_TEXT_FILTER,
  filter
});

export const VisibilityFilters = {
  SHOW_ALL: "SHOW_ALL",
  FILTER_BY_NAME: "FILTER_BY_NAME"
};
