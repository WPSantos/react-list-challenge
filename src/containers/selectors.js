export const getUsersState = store => store.users;

export const getUserList = store =>
  getUsersState(store) ? getUsersState(store).allIds : [];

export const getUserById = (store, id) =>
  getUsersState(store) ? { ...getUsersState(store).byIds[id], id } : {};

export const getUsers = store =>
  getUserList(store).map(id => getUserById(store, id));

export const getUsersByFilter = store => {
  const allUsers = getUsers(store);

  if (!store.userFilter) return allUsers;
  else
    return allUsers.filter(
      user => user.content.name.indexOf(store.userFilter) > -1
    );
};
