import { connect } from "react-redux";
import { setTextFilter } from "../actions";
import Text from "../components/Text";

const mapDispatchToProps = dispatch => ({
  onChange: e => dispatch(setTextFilter(e.currentTarget.value))
});

export default connect(
  null,
  mapDispatchToProps
)(Text);
