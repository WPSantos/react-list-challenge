import { combineReducers } from "redux";
import { userFilter } from "./userFilter";
import users from "./users";

export default combineReducers({ users, userFilter });
