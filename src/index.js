import React from "react";
import ReactDOM from "react-dom";

import { Provider } from "react-redux";
import store from "./store";

import UserListApp from "./UserListApp";

const rootElement = document.getElementById("root");
ReactDOM.render(
  <Provider store={store}>
    <UserListApp />
  </Provider>,
  rootElement
);
