import React from "react";
import { connect } from "react-redux";
import { addUser } from "../actions";

import AppBar from "@material-ui/core/Appbar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";

class NavBar extends React.Component {
  constructor(props) {
    super(props);
    this.componentDidMount();
  }

  async componentDidMount() {
    const url = "https://jsonplaceholder.typicode.com/users";
    const response = await fetch(url);
    const data = await response.json();

    data.forEach(element => {
      this.props.addUser(element);
    });
  }

  render() {
    return (
      <div>
        <AppBar position="static">
          <Toolbar>
            <Typography color="inherit">React List Challenger</Typography>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

export default connect(
  null,
  { addUser }
)(NavBar);
