import React from "react";
import PropTypes from "prop-types";
import TextField from "@material-ui/core/TextField";

const Text = ({ onChange }) => (
  <TextField
    id="standard-name"
    label="Name"
    onChange={onChange}
    margin="normal"
  />
);

Text.propTypes = {
  onChange: PropTypes.func.isRequired
};

export default Text;
