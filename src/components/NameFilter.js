import React from "react";
import FilterText from "../containers/FilterText";

const NameFilter = () => (
  <div>
    <FilterText />
  </div>
);

export default NameFilter;
