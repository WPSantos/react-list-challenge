import React from "react";
import { connect } from "react-redux";

import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";

const User = ({ user }) => (
  <TableRow key={user.id}>
    <TableCell component="th" scope="row">
      {user.id}
    </TableCell>
    <TableCell align="left">{user.content.name}</TableCell>
    <TableCell align="left">{user.content.username}</TableCell>
    <TableCell align="left">{user.content.email}</TableCell>
    <TableCell align="left">{user.content.company.name}</TableCell>
  </TableRow>
);

export default connect(null)(User);
