import React from "react";
import NavBar from "./components/NavBar";
import NameFilter from "./components/NameFilter";
import UserList from "./components/UserList";

export default function UserListApp() {
  return (
    <div className="userlist-app">
      <NavBar />
      <NameFilter />
      <UserList />
    </div>
  );
}
